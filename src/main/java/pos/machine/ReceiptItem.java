package pos.machine;

public class ReceiptItem {
    private Item item;

    private int quantity;

    private int unitPrice;

    public ReceiptItem(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
        this.unitPrice = item.getPrice() * quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }
}

package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        String lastReceipt = renderReceipt(receipt);
        return lastReceipt;
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes){
        List<Item> items = ItemsLoader.loadAllItems();
        Map<String,Integer> barcodeWithQuantity = new HashMap<>();
        for (String barcode : barcodes) {
            if (barcodeWithQuantity.containsKey(barcode)){
                barcodeWithQuantity.put(barcode,barcodeWithQuantity.get(barcode)+1);
            } else {
              barcodeWithQuantity.put(barcode,1);
            }
        }
        return getReceiptItems(barcodeWithQuantity,items);
    }

    public List<ReceiptItem> getReceiptItems(Map<String,Integer> barcodeWithQuantity,List<Item> items){
        List<ReceiptItem> receiptItems = new ArrayList<>();
        for(Map.Entry<String,Integer> entry : barcodeWithQuantity.entrySet()){
            Item item = findByBarcode(entry.getKey(),items);
            ReceiptItem receiptItem = new ReceiptItem(item,entry.getValue());
            receiptItems.add(receiptItem);
        }
        return receiptItems;
    }

    public Item findByBarcode(String barcode,List<Item> items){
        for (Item item : items) {
            if(item.getBarcode() == barcode){
                return item;
            }
        }
        return null;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems){
        return new Receipt(receiptItems,calculateTotalPrice(receiptItems));
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems){
        int count = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            count += receiptItem.getUnitPrice();
        }
        return count;
    }

    public String renderReceipt(Receipt receipt){
        String itemsReceipt = "";
        for (ReceiptItem receiptItem : receipt.getReceiptItems()) {
            String itemReceipt = generateItemsReceipt(receiptItem);
            itemsReceipt += itemReceipt + "\n";
        }
        return generateReceipt(receipt,itemsReceipt);
    }
    public String generateItemsReceipt(ReceiptItem receiptItem){
        return "Name: " + receiptItem.getItem().getName() + ", Quantity: " +
                receiptItem.getQuantity() + ", Unit price: " + receiptItem.getItem().getPrice()
                + " (yuan), Subtotal: " + receiptItem.getUnitPrice() + " (yuan)";
    }

    public String generateReceipt(Receipt receipt,String receiptItem){
        String lastReceipt = "***<store earning no money>Receipt***\n";
        lastReceipt += receiptItem;
        lastReceipt += "----------------------\n"+
                "Total: " + receipt.getSubTotal() + " (yuan)\n" +
                "**********************";
        return lastReceipt;
    }
}

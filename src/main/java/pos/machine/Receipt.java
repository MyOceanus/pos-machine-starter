package pos.machine;

import java.util.List;

public class Receipt {
    private List<ReceiptItem> receiptItems;

    private int subTotal;

    public Receipt(List<ReceiptItem> receiptItems, int subTotal) {
        this.receiptItems = receiptItems;
        this.subTotal = subTotal;
    }

    public List<ReceiptItem> getReceiptItems() {
        return receiptItems;
    }

    public void setReceiptItems(List<ReceiptItem> receiptItems) {
        this.receiptItems = receiptItems;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }
}
